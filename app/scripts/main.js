'use strict';

// ready
$(document).ready(function() {

  // anchor
  $(".anchor").on("click", "a", function(event) {
    event.preventDefault();
    var id = $(this).attr('href'),
      top = $(id).offset().top;
    $('body,html').animate({
      scrollTop: top + 40
    }, 1000);
  });
  // anchor

  // adaptive menu
  $('.main-nav__toggle--js').click(function() {
    $(this).next().slideToggle();
  });
  $('.main-nav__item--parent > a').click(function() {
    $(this).next().slideToggle();
    $(this).toggleClass('act');
    return false;
  });
  // adaptive menu

  // calc open
  $('.calc-section-br').click(function() {
    $(this).toggleClass('act');
    $('.calc-section-hide').slideToggle();
  });
  // calc open

  // mask phone {maskedinput}
  $("[name=phone]").mask("+ 7 (999) 999-99-99");
  // mask phone

  // slider {slick-carousel}
  $('.slider-rev').slick({
    dots: true,
    infinite: true,
    adaptiveHeight: true,
    slidesToShow: 2,
    slidesToScroll: 2,
    appendArrows: ".slider-arrow-r",
    nextArrow: '<div class="slider-arrow-left slick-arrow"><i class="is-icons is-icons--next-arr"></i></div>',
    prevArrow: '<div class="slider-arrow-right slick-arrow"><i class="is-icons is-icons--prev-arr"></i></div>',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToScroll: 1,
          slidesToShow: 1
        }
      }
    ]
  });
  var filterSlider = $('.filtering');
  filterSlider.slick({
      slide: ".option",
      dots: true,
      adaptiveHeight: true,
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      appendArrows: ".slider-arrow",
      nextArrow: '<div class="slider-arrow-left slick-arrow"><i class="is-icons is-icons--next-arr"></i></div>',
      prevArrow: '<div class="slider-arrow-right slick-arrow"><i class="is-icons is-icons--prev-arr"></i></div>',
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToScroll: 2,
            slidesToShow: 2
          }
        },
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            slidesToScroll: 1,
            slidesToShow: 1
          }
        }
      ]
  });
  $('.js-filter-mat').on('click', function () {
      $(this).siblings().removeClass('active');
      filterSlider.slick('slickUnfilter');
      filterSlider.slick('slickFilter', '.filter-mat');
      $(this).addClass('active');
      return false;
  });
  $('.js-filter-gl').on('click', function () {
      $(this).siblings().removeClass('active');
      filterSlider.slick('slickUnfilter');
      filterSlider.slick('slickFilter', '.filter-gl');
      $(this).addClass('active');
      return false;
  });
  $('.js-filter-sat').on('click', function () {
      $(this).siblings().removeClass('active');
      filterSlider.slick('slickUnfilter');
      filterSlider.slick('slickFilter', '.filter-sat');
      $(this).addClass('active');
      return false;
  });
  $('.js-filter-all').on('click', function () {
      $(this).siblings().removeClass('active');
      filterSlider.slick('slickUnfilter');
      $(this).addClass('active');
      return false;
  });
  // slider

  // select {select2}
  $('select').select2({
    minimumResultsForSearch: Infinity
  });
  // select

  // popup {magnific-popup}
  $('.popup').magnificPopup();
	$(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});
  // popup


});
// ready

// mobile sctipts
var screen_width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
if (screen_width <= 767) {}
// mobile sctipts


function onProjectChange() {
    var list_c = document.getElementById('factura');
    var list_m = document.getElementById('color');
    var dis = list_c.value;
    if(dis != 4) {
        list_m.disabled = false;
    }
    else {
        list_m.value = 0;
        list_m.disabled = true;
    }
}

jQuery('#square, #factura, #color, #qty_corners, #qty_pipes, #qty_biglight, #qty_light').on('input', function(){
    var itog;
    var color = $('#color').val();
    var square = $('#square').val();
    var qty_corners = $('#qty_corners').val();
    var qty_pipes = $('#qty_pipes').val();
    var qty_biglight = $('#qty_biglight').val();
    var qty_light = $('#qty_light').val();
    var factura = $('#factura').val();

    if (color == "0") {
      itog = "<span class='text--danger'>Не выбран цвет</span>";
    }
    if (color == "1") {
      if (square <= "38") {
        var s1 = square * 250; // Стоимость  Потолка
        var s2 = square * 350; // Стоимость погонного метра
        var s3 = qty_corners * 100; // Стоимость по кол-ву углов
        var s4 = qty_pipes * 350; // Стоимость по кол-ву труб
        var s5 = qty_biglight * 500; // Стоимость по кол-ву люстр
        var s6 = qty_light * 450; // Стоимость по кол-ву светильников
        itog = s1 + s2 + s3 + s4 + s5 + s6;
      } else {
        var s1 = square * 100; // Стоимость  Потолка
        var s2 = square * 350; // Стоимость погонного метра
        var s3 = qty_corners * 100; // Стоимость по кол-ву углов
        var s4 = qty_pipes * 350; // Стоимость по кол-ву труб
        var s5 = qty_biglight * 500; // Стоимость по кол-ву люстр
        var s6 = qty_light * 450; // Стоимость по кол-ву светильников
        itog = s1 + s2 + s3 + s4 + s5 + s6;
      }
    }
    if (color == "2") {
      if (square <= "38") {
        var s1 = square * 310; // Стоимость  Потолка
        var s2 = square * 350; // Стоимость погонного метра
        var s3 = qty_corners * 100; // Стоимость по кол-ву углов
        var s4 = qty_pipes * 350; // Стоимость по кол-ву труб
        var s5 = qty_biglight * 500; // Стоимость по кол-ву люстр
        var s6 = qty_light * 450; // Стоимость по кол-ву светильников
        itog = s1 + s2 + s3 + s4 + s5 + s6;
      } else {
        var s1 = square * 150; // Стоимость  Потолка
        var s2 = square * 350; // Стоимость погонного метра
        var s3 = qty_corners * 100; // Стоимость по кол-ву углов
        var s4 = qty_pipes * 350; // Стоимость по кол-ву труб
        var s5 = qty_biglight * 500; // Стоимость по кол-ву люстр
        var s6 = qty_light * 450; // Стоимость по кол-ву светильников
        itog = s1 + s2 + s3 + s4 + s5 + s6;
      }
    }
    if (factura == "0") {
      itog = "<span class='text--danger'>Не выбрана фактура</span>";
    }
    if (factura == "4") {
      if (square <= "38") {
        var s1 = square * 1100; // Стоимость  Потолка
        var s2 = square * 350; // Стоимость погонного метра
        var s3 = qty_corners * 100; // Стоимость по кол-ву углов
        var s4 = qty_pipes * 350; // Стоимость по кол-ву труб
        var s5 = qty_biglight * 500; // Стоимость по кол-ву люстр
        var s6 = qty_light * 450; // Стоимость по кол-ву светильников
        itog = s1 + s2 + s3 + s4 + s5 + s6;
      } else {
        var s1 = square * 800; // Стоимость  Потолка
        var s2 = square * 350; // Стоимость погонного метра
        var s3 = qty_corners * 100; // Стоимость по кол-ву углов
        var s4 = qty_pipes * 350; // Стоимость по кол-ву труб
        var s5 = qty_biglight * 500; // Стоимость по кол-ву люстр
        var s6 = qty_light * 450; // Стоимость по кол-ву светильников
        itog = s1 + s2 + s3 + s4 + s5 + s6;
      }
    }
    if (itog) {
      jQuery("#result_div_id").html(itog);
    } else {
      jQuery("#result_div_id").html('<span class="text--danger">Укажите правильные данные</span>');
    }

});
